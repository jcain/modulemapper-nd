# ModuleMapper ND

ModuleMapper ND is a [Service Worker](https://developer.mozilla.org/en-US/docs/Web/API/Service_Worker_API) to dyamically map nodejs module exports to the expected paths.


## Component stability statuses

| Component          | Stability | Since |
|:-------------------|:---------:|:-----:|
| modulemapper-nd.js | alpha     | 0.0   |

The **Stability** column indicates the component's stability status, and the **Since** column indicates the package version when the component first achieved that stability.

Each component and its members has a **stability status** indicating how stable the interface and implementation is to depend on in production. The stability may be one of the following:

* **alpha**: The interface and implementation are unstable and may change significantly.
* **beta**: The interface is stable but its implementation is not sufficiently tested.
* **omega**: The interface and implementation are stable and considered ready for production use.

A component's stability status is the same as the highest stability status of its members. Once a member's stability is raised, it will not be reduced.


## License

ModuleMapper ND is licensed under the MIT license. See the [LICENSE](LICENSE.md) file for more information.