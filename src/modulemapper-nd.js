const parsed = new Set();
const mappings = [];

const basePath = new URL(registration.scope + "node_modules/").pathname;

self.addEventListener("fetch", (evt) => {
	let url = new URL(evt.request.url);

	let mapped = getMapped(url.pathname);
	if (!mapped) {
		let parts = url.pathname.substr(basePath.length).split("/").filter((s) => !!s);

		let modulePath = parts[0] + "/" + (parts[0][0] === "@" ? parts[1] + "/" : "");

		if (parsed.has(modulePath)) {
			evt.respondWith(fetch(url));
			return;
		}

		let url2 = new URL(url);
		url2.pathname = basePath + modulePath + "package.json";

		evt.respondWith(
			fetch(url2)
			.then((response) => response.json())
			.then((json) => {
				let beforeUrl = new URL(url);
				let afterUrl = new URL(url);

				let exports = json.exports;
				for (let before in exports) {
					let after = exports[before];

					beforeUrl.pathname = basePath + modulePath + before;
					afterUrl.pathname = basePath + modulePath + after;

					before = new URL(beforeUrl).pathname;
					after = new URL(afterUrl).pathname;

					mappings.push({
						before,
						after,
						isFolder: before[before.length - 1] === "/",
					});
				}

				mappings.sort((a, b) => b.before.length - a.before.length || a.before.localeCompare(b.before));

				mapped = getMapped(url.pathname);
				if (mapped)
					url.pathname = mapped;

				return fetch(url);
			})
		);
	}
	else {
		url.pathname = mapped;
		evt.respondWith(fetch(url));
	}
});

// ~~~~~~~~~~


function getMapped(path) {
	for (let mapping of mappings) {
		let before = mapping.before;
		let after = mapping.after;

		if (before.length > path.length)
			continue;

		if (path.startsWith(before))
			return after + path.substr(before.length);
	}

	return undefined;
}